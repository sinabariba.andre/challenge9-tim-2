# FSW - Chapter 9 - Challenge

## Prasyarat

Sebelum memulai menyentuh repository, pastikan komputer teman-teman memenuhi prasyarat berikut:

- sudah install Node.js & NPM
- sudah install git

## Project setup

1. Clone Repo
   $ git clone https://gitlab.com/sinabariba.andre/challenge9-tim-2.git

2. Pindah ke folder repository

```
$ cd challenge9-tim-2
```

3. Install dependencies

```
$ npm install
```

## Git PULL

1. Cara pull:

```
$ git pull origin master
```


### Run

Untuk menjalankan aplikasi

```
$ yarn start
```
