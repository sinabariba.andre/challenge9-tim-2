import React from 'react'
import {BrowserRouter , Route, Switch} from 'react-router-dom'
import homePage from './components/pages/homepage/homePage'
import registerPage from './components/pages/register/registerPage'
import loginPage from './components/pages/login/loginPage'
import NotFound from './components/pages/NotFound'
import landingPage from './components/pages/landingpage/landingPage'
import profilePage from './components/pages/profile/profilePage'
import gameList from './components/pages/game-list/gameList'
import gameDetail from './components/pages/game-detail/gameDetail'
import NavbarComp from './components/pages/navbar/navbarComp'
import userList from './components/pages/userList/userList'
import Create from './components/pages/Create'



const Router = (props)=> (
    
    <BrowserRouter>
    
        <NavbarComp/>
        <Switch>
            <Route exact  path="/" component={homePage} />
            <Route exact authed={props.authed} path="/register" component={registerPage} />
            <Route exact authed={props.authed} path="/login" component={loginPage} />
            <Route authed={props.authed} exact path="/dashboard" component={landingPage} />
            <Route authed={props.authed} exact path="/profile" component={profilePage} />
            <Route authed={props.authed} exact path="/game-list" component={gameList} />
            <Route authed={props.authed} exact path="/game-detail" component={gameDetail} />
            <Route authed={props.authed} exact path="/user-list" component={userList} />
            <Route exact path="/create-game-list" component={Create} />
            {/* <Route exact path="/dasboard" component={dashboardPage} /> */}
            <Route component={NotFound} />
        </Switch>
    </BrowserRouter>
)

export default Router
