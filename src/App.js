import './App.css';
import Router from './Router';
import { AuthProvider } from './providers/AuthContext'
import { Component } from 'react';



class App extends Component {
  render(){

    return (
      <AuthProvider>
      <Router/>
    </AuthProvider>
    
  //  <div className="container-small">
  //    <Router/>
  //  </div>
    
  );
}
}

export default App;
