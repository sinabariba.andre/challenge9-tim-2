import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/storage'
import 'firebase/database'

const firebaseConfig = {
    apiKey: "AIzaSyAecLKqeS7DE0Zza6G_Vooh6AidUz-iX8I",
    authDomain: "react-firebase-62896.firebaseapp.com",
    projectId: "react-firebase-62896",
    storageBucket: "react-firebase-62896.appspot.com",
    messagingSenderId: "908749369823",
    appId: "1:908749369823:web:630de626959a6f8b83546a",
    measurementId: "G-5GWD6B93PG"
  };
// const firebaseConfig = {
//   apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
//   authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
//   projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
//   storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
//   messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
//   appId: process.env.REACT_APP_FIREBASE_API_ID,
//   measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
// };
const app = firebase.initializeApp(firebaseConfig)

export default app