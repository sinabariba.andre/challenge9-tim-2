import React,{ Component } from 'react'
import {Link} from 'react-router-dom'
import Firebase from '../../../services/firebase'

import {Route, Redirect,} from 'react-router-dom'

import {Collapse, Navbar,NavbarToggler,NavbarBrand,Nav,NavItem, } from 'reactstrap';


function PrivateRoute ({component: Component, authed, ...rest}) {
  return (
    <Route
      {...rest}
      render={(props) => authed === true
        ? <Component {...props} />
        : <Redirect to={{pathname: '/login', state: {from: props.location}}} />}
    />
  )
}

function PublicRoute ({component: Component, authed, ...rest}) {
  return (
    <Route
      {...rest}
      render={(props) => authed === false
        ? <Component {...props} />
        : <Redirect to='/dashboard' />}
    />
  )
}
class  NavbarComp extends Component {

  
    state={
      isOpen : false,
      authed:false,
      loading:true
    }

    toggle (){
      this.setState({
        isOpen: true
      })
    }

    logout = async () => {
      await Firebase.auth().signOut();
      alert('Berhasil logout!')
    }
    
    componentDidMount(){
      this.removeListener =Firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          this.setState({
            authed: true,
            loading: false,
          })
        } else {
          this.setState({
            authed: false,
            loading: false
          })
        }
      })
    }
    // componentWillUnmount () {
    //   this.removeListener()
    // }

    // const [isOpen, setIsOpen] = useState(false);
    // const toggle = () => setIsOpen(!isOpen);
    render(){
    return this.state.loading === true ? <h1>Loading</h1> : (
            <div className="">
              <Navbar className="box p-3" color="" light expand="md">
                <NavbarBrand className="mr-5 pr-5" href="/">Binar Game</NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                  <Nav className="mr-auto nav-custom" navbar>
                    <NavItem className="mr-5 pr-5 pt-2">
                       <Link to="/">Home</Link>
                    </NavItem>
                    <NavItem className="mr-5 pr-5 pt-2">
                    {this.state.authed && <Link to="/dashboard" className="mr-5 pr-5 pt-2">Dashboard</Link>}
                      
                    </NavItem>
                    
                  {this.state.authed ? 
                  <NavItem>  
                      <Link style={{border:'1px, red, solid'}} to="/profile" className="mr-5 pr-5 pt-2">My Profile</Link>
                    <Link to="/game-list" className="mr-5 pr-5 pt-2">Game List</Link>
                  
                   <Link to="/user-list" className="mr-5 pr-5 pt-2">User List</Link>
                    <button style={{border: 'none', background: 'transparent', color:'white'}} onClick={() => {
                      this.logout()
                    }}  className="mr-5 pr-5 pt-2">Log Out</button>
                    </NavItem>
                    : <NavItem>
                        <Link to="/login" className="mr-5 pr-5 pt-2">Login</Link>
                        <Link to="/register" className="mr-5 pr-5 pt-2">Register</Link>
                      </NavItem>
              

                      }
                    {/* <NavItem className="mr-5 pr-5 pt-2">
                      <Link to="/login">Login</Link>
                    </NavItem> 
                    <NavItem className="mr-5 pr-5 pt-2">
                      <Link to="/register">Register</Link>
                    </NavItem>  */}
                  </Nav>
                  
                </Collapse>
              </Navbar>
            </div>
            
    )
}
}
export default NavbarComp
