import React, { Component } from 'react'
import firebase from '../../services/firebase'
import {AuthContext} from '../../providers/AuthContext'
import { withRouter } from 'react-router'
import { FormGroup ,Input } from 'reactstrap'

class Create extends Component {
    static contextType = AuthContext
    
    state = {}

    set = name => e =>{
        this.setState ({ 
            [name]:e.target.value
        })
    }
    handleSubmit = async e =>{
        e.preventDefault()
        const { currentUser } = this.context
        const { history } = this.props
        const { game, desc } = this.state

        if(!game || !desc) return alert("Please write something")
        if(!currentUser) return history.push('/login')

        try {
            await firebase
            .database().ref(`gamelist/${Date.now()}`).set({game:game, desc:desc})
            alert('Posted')
        } catch (error) {
            console.log(error)
            alert('Failed to Post')
        }
    }

    render() {
        return (
            <div className="container mt-5">
                <header className="App">
                <form onSubmit={this.handleSubmit} className="form-custom">
                    <h4>Write your story here!</h4>
                    <FormGroup>
                        <Input value={this.state.value} onChange={this.set('game')} />
                    </FormGroup>
                    <FormGroup>
                        <textarea value={this.state.value} onChange={this.set('desc')} />
                    </FormGroup>
                    <div className="form-button">
                        <Input type="submit" value="Post" className="form-submit" />
                    </div>
                </form>
                </header>
            </div>
        )
    }
}
export default withRouter(Create)