import React, { Component } from 'react'
import { Container, Form, FormGroup, Label, Input } from 'reactstrap';
import firebase from '../../../services/firebase'

export default class registerpage extends Component {
  state={}

  set = name => event =>{
      console.log(name)
      this.setState({
          [name]:event.target.value
      })
  }

  handleRegister = async event =>{
      const { email, password ,username } = this.state
      event.preventDefault()
      if(!email ||!username || !password){
          return alert ('Email or password cannot be empty')
      }
        

        try {
            await firebase
            .database().ref(`users/${Date.now()}`).set({email:email, username:username})
            alert('Berhasil Daftar')
        } catch (error) {
            console.log(error)
            alert('Failed to Post')
        }
      firebase.auth().createUserWithEmailAndPassword(email,password)
      .then((userCredential)=>{
          const user = userCredential.user
          console.log(user)
          alert(`User has been successfully registerd :${user.email}`)
      })
      .catch((error)=>{
          const errorCode = error.code
          const errorMessage = error.errorMessage
          alert(`User has been failed registerd :${errorCode} + ${errorMessage}`)
      })
  }
    render() {
        return (
            <Container className="d-flex justify-content-center mt-5">
            <Form className="col-md-5 form-custom" onSubmit={this.handleRegister}>
              <h2 className="App mb-5 mt-5">Create Your Account</h2>
                  <FormGroup>
                    <Label for="username">Username</Label>
                    <Input type="text" name="username" onChange={this.set('username')} id="username" placeholder="John Thor" />
                  </FormGroup>
                  <FormGroup>
                    <Label for="email">Email</Label>
                    <Input type="email" name="email" onChange={this.set('email')} id="email" placeholder="example@gmail.com" />
                  </FormGroup>
                  <FormGroup>
                    <Label for="password">Password</Label>
                    <Input type="password" name="password" onChange={this.set('password')} id="password" placeholder="* * * * * * * *" />
                  </FormGroup>
                  <div className="App">

                  <button type="submit" className="btn button-custom mb-5">Submit</button>
                  </div>
          </Form>
          
        </Container>
        )
    }
}
